<?php
//Advanced Custom fields registering Options sub-pages
if ( function_exists( "register_options_page" ) ) {
	register_options_page( 'General' );
	register_options_page( 'Contact' );
	register_options_page( 'Pages' );
	register_options_page( 'Woocommerce' );
}
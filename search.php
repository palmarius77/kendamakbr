<?php
//Index file for displaying search results
get_header();

$ppp   = 8;
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : '1';
?>

  <div class="blog-page inner-page">

		<?php require_once 'wp-partials/top-banner.php'; ?>

    <div class="page-wrapper default-page">
      <div class="container">

				<?php require_once 'wp-partials/breadcrumb.php'; ?>

        <div class="page-content">
          <div class="row articles-listing ajax_posts search_results">

						<?php
						$args  = array(
								'posts_per_page' => $ppp,
								'paged'          => $paged,
								's'              => get_query_var( 's' )
						);
						$query = new WP_Query( $args );
						if ( $query->have_posts() ) {
							while ( $query->have_posts() ) {
								$query->the_post();
								postTeaser();
							}
						} else {
							echo '<h2>' . __( 'No posts found', 'kendamakbr' ) . '</h2>';
						}
						?>

          </div>
					<?php
					$args_2                   = $args;
					$args_2['posts_per_page'] = - 1;
					$query_2                  = new WP_Query( $args_2 );
					$max                      = $query_2->post_count;
					if ( $max > $ppp ):
						?>
            <div class="centered-content small-mt">
              <a href="#" title="" class="button load-more-btn" id="more_posts"
                 data-ppp="<?php echo $ppp; ?>"
                 data-post_type="all"
                 data-s="<?php echo get_query_var( 's' ); ?>">
								<?php _e( 'Load More', 'kendamakbr' ); ?>
              </a>
            </div>
					<?php endif; ?>
        </div><!-- End .page-content -->

      </div>
    </div>

  </div>

<?php get_footer(); ?>
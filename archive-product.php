<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see      https://docs.woocommerce.com/document/template-structure/
 * @author    WooThemes
 * @package  WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
get_header( 'shop' );
?>

  <div class="shop-page inner-page">
		<?php require_once 'wp-partials/top-banner.php'; ?>

    <div class="page-wrapper">
      <div class="container">

				<?php require_once 'wp-partials/breadcrumb.php'; ?>


        <div class="page-content floating-objects">

					<?php require_once 'wp-partials/product-archive-sidebar.php'; ?>

          <div class="main-content">

						<?php
						/**
						 * Hook: woocommerce_before_shop_loop.
						 *
						 * @hooked wc_print_notices - 10
						 * @hooked woocommerce_result_count - 20 --> removed
						 * @hooked woocommerce_catalog_ordering - 30 --> removed
						 */
						do_action( 'woocommerce_before_shop_loop' );

						require_once 'wp-partials/product-archive-top-filters.php';

						if ( have_posts() ) {

							if ( wc_get_loop_prop( 'total' ) ) { ?>
                <div class="products-listing row big-gutter">
									<?php while ( have_posts() ) {
										the_post();

										/**
										 * Hook: woocommerce_shop_loop.
										 *
										 * @hooked WC_Structured_Data::generate_product_data() - 10
										 */
										do_action( 'woocommerce_shop_loop' ); ?>
                    <div class="col_33">
											<?php wc_get_template_part( 'content', 'product' ); ?>
                    </div>
										<?php
									} ?>
                </div><!-- products-listing row big-gutter -->
								<?php
							}

							/**
							 * Hook: woocommerce_after_shop_loop.
							 *
							 * @hooked woocommerce_pagination - 10 --> removed
							 */
							do_action( 'woocommerce_after_shop_loop' );
							?>

              <div class="centered-content small-mt small-mb">
                <a href="#" class="button" id="load-more-products"
                   data-prpp="<?php echo get_field( 'products_per_page', 'options' ); ?>"><?php _e( 'Load More', 'kendamakbr' ); ?></a>
              </div>

							<?php
						} else {
							/**
							 * Hook: woocommerce_no_products_found.
							 *
							 * @hooked wc_no_products_found - 10
							 */
							do_action( 'woocommerce_no_products_found' );
						}

						/**
						 * Hook: woocommerce_after_main_content.
						 *
						 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
						 */
						do_action( 'woocommerce_after_main_content' );
						?>

          </div><!-- main content -->

        </div><!-- page-content floating-objects -->

      </div><!-- container -->
    </div><!-- page-wrapper -->

  </div><!-- shop-page inner-page -->

<?php
get_footer( 'shop' );

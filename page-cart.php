<?php
/*
Template Name: Cart
*/
?>
<?php
global $woocommerce;
if ( WC()->cart->cart_contents_count == 0 ) {
	wp_safe_redirect( get_permalink( woocommerce_get_page_id( 'shop' ) ) );
} else {
	wp_safe_redirect( $woocommerce->cart->get_checkout_url() );
}
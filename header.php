<!DOCTYPE html>
<html>
<head>
  <title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) {
			echo ' |';
		} ?><?php bloginfo( 'name' ); ?></title>
  <link rel="alternate" type="application/rss+xml"
        title="<?php bloginfo( 'name' ); ?> RSS Feed"
        href="<?php bloginfo( 'rss2_url' ); ?>"/>
  <link rel="alternate" type="application/rss+xml"
        title="<?php bloginfo( 'name' ); ?> Comments RSS Feed"
        href="<?php bloginfo( 'comments_rss2_url' ) ?>"/>
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>

	<?php wp_head() ?>

  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
  <meta http-equiv="Content-Type"
        content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>"/>
  <meta name="generator" content="WordPress <?php bloginfo( 'version' ) ?>"/>

</head>
<body <?php body_class( $class ); ?>>

<?php
global $woocommerce, $td_uri, $cur, $cur_simb;
?>

<div id="main-scrollbar" data-scrollbar>

  <header id="header" class="site-header">
    <div class="container floating-objects">

      <a href="<?php echo get_bloginfo( 'url' ); ?>" title="<?php echo get_bloginfo( 'name' ); ?>" class="logo left">
				<?php $img = get_field( 'logo', 'options' ); ?>
        <img src="<?php echo $img['url']; ?>" onerror="this.src='images/logo.png'">
      </a>

      <a href="#" class="open-sidemenu right" title=""><span></span><span></span><span></span></a>

      <div class="language-selector right">
        <a href="#">
          <img src="<?php echo $td_uri; ?>/images/english-flag.png" alt="">
        </a>
        <a href="<?php the_field( 'romanian_website', 'options' ); ?>">
          <img src="<?php echo $td_uri; ?>/images/romanian-flag.png" alt="">
        </a>
      </div>

      <div class="header-options right">

        <div class="search-form">
          <form role="search" method="get" action="<?php home_url( '/shop/' ); ?>">
            <input type="text" name="s" value="<?php echo get_search_query(); ?>" placeholder="<?php _e( 'Search...', 'kendamakbr' ); ?>">
          </form>
        </div>

        <a href="#" title="" class="open-search-form"><i class="fas fa-search"></i></a>

        <a href="<?php $pg = get_field( 'wishlist_page', 'options' );
				echo $pg->guid; ?>" class="open-wishlist"><i class="fas fa-heart"></i></a>

        <div class="dropdown-cart-section">
          <a href="<?php echo $woocommerce->cart->get_cart_url(); ?>" class="open-cart-dropdown cart_anchor">
            <i class="fas fa-shopping-cart"><span><?php echo $woocommerce->cart->cart_contents_count; ?></span></i>
          </a>
					<?php if ( $woocommerce->cart->cart_contents_count ) { ?>
            <div class="dropdown-cart-wrapper">
              <div class="cart-wrapper">
								<?php
								$items = $woocommerce->cart->get_cart();
								foreach ( $items as $item => $values ) {
									$_product = wc_get_product( $values['data']->get_id() );
									$price    = $_product->price;
									?>
                  <div class="product-row">
										<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $values['product_id'] ), 'gallery_small' ); ?>
                    <a href="<?php echo get_permalink( $values['product_id'] ); ?>" class="image">
                      <img src="<?php echo $image[0]; ?>" alt="thumbnail">
                    </a>
                    <span><a href="<?php echo get_permalink( $values['product_id'] ); ?>"><?php echo $values['data']->name; ?></a></span>
                    <span>x<?php echo $values['quantity']; ?></span>
                    <span class="price"><?php echo $cur_simb . $price; ?></span>
                    <a href="#" data-id="<?php echo $values['product_id']; ?>" data-vid="<?php echo $values['data']->variation_id; ?>" class="delete-item"><i
                          class="fas fa-times-circle"></i></a>
                  </div>
								<?php } ?>
                <div class="floating-objects small-mt">
                  <a href="<?php echo $woocommerce->cart->get_cart_url(); ?>"
                     class="button small right"><?php _e( 'View cart', 'kendamakbr' ); ?></a>
                </div>
              </div>
            </div>
					<?php } ?>
        </div>

      </div>
      <nav class="navigation-menu right">
				<?php
				wp_nav_menu( [
						'theme_location' => is_user_logged_in() ? 'header_logged_in' : 'header',
						'container'      => '',
						'walker'         => new Header_Walker(),
				] );
				?>
      </nav>

    </div>
    <div class="big-menu-wrapper">
      <div class="container">
				<?php
				wp_nav_menu( [
						'theme_location' => is_user_logged_in() ? 'header_mobile_logged_in' : 'header_mobile',
						'container'      => '',
						'walker'         => new Mobile_Walker(),
				] );
				?>
      </div>
    </div>

  </header>

	<?php require_once 'wp-partials/header-popups.php'; ?>
	<?php require_once 'wp-partials/intro-popup.php'; ?>

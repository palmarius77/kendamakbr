<?php
/*
Template Name: Contact
*/
?>
<?php get_header();
wp_reset_query(); ?>

  <div class="contact-page inner-page">

		<?php require_once 'wp-partials/top-banner.php'; ?>

    <div class="page-wrapper default-page">
      <div class="container">

				<?php require_once 'wp-partials/breadcrumb.php'; ?>

        <div class="page-content">

          <div class="row big-gutter">
            <div class="col_60">
              <div class="grey-box big-pd">
								<?php
								the_content();
								echo do_shortcode( '[contact-form-7 id="8" title="Contact page"]' );
								?>
              </div>
            </div>
            <div class="col_40">
              <h4><?php _e( 'Email Address', 'kendamakbr' ); ?></h4>
              <p><a href="mailto:<?php the_field( 'email', 'options' ); ?>"><?php the_field( 'email', 'options' ); ?></a></p>
              <h4><?php _e( 'Phone Number', 'kendamakbr' ); ?></h4>
              <p><?php the_field( 'phone', 'options' ); ?></p>
							<?php the_field( 'other_contact', 'options' );
							$img = get_field( 'logo_2', 'options' );
							?>
              <div class="centered-content company-logo"><img src="<?php echo $img['url']; ?>" alt="<?php echo get_bloginfo( 'name' ); ?>"></div>
            </div>
          </div>

        </div><!-- End .page-content -->

      </div>
    </div>

  </div>

<?php get_footer(); ?>
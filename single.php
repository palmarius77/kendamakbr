<?php
/**
 * The Template for displaying all single posts.
 */
get_header();
wp_reset_query(); ?>

<div class="article-page inner-page">

	<?php require_once 'wp-partials/top-banner.php'; ?>

  <div class="page-wrapper default-page">
    <div class="container">

			<?php require_once 'wp-partials/breadcrumb.php'; ?>

      <div class="page-content">

        <div class="article-featured-img">
					<?php the_post_thumbnail( 'large' ); ?>
        </div>

        <div class="limited-content">
          <div class="article-meta">
            <span><i class="far fa-clock"></i> <?php echo get_the_date(); ?></span>
						<?php $categories = get_the_category(); ?>
            <span><a href="<?php echo get_category_link( $categories[0]->term_id ); ?>">
                <i class="far fa-folder-open"></i> <?php echo $categories[0]->cat_name; ?>
              </a></span>
          </div>

          <div class="article-content">
						<?php the_content(); ?>
          </div>

          <div class="floating-objects small-mt">
						<?php
						$tags = get_tags();
						if ( $tags ): ?>

              <div class="tags-section left">
                <h4><?php _e( 'Tags', 'kendamakbr' ); ?></h4>

                <div class="tags-wrapper">
									<?php
									foreach ( $tags as $tag ) {
										$tag_link = get_tag_link( $tag->term_id );
										echo '<a href="' . $tag_link . '">' . $tag->name . '</a>';
									} ?>
                </div>

              </div>
						<?php endif; ?>

            <div class="share-section right">
              <h4><?php _e( 'Share', 'kendamakbr' ); ?></h4>
              <?php echo do_shortcode('[addtoany]'); ?>
            </div>

          </div>
        </div>

        <div class="sep"></div>

	      <?php require_once 'wp-partials/similar-posts.php'; ?>

      </div><!-- End .page-content -->

    </div>
  </div>

</div>

<?php get_footer(); ?>

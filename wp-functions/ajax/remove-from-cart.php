<?php
function remove_item_from_cart() {
	global $woocommerce;
	$id = $_POST['product_id'];
	$vid = $_POST['variation_id'];
	foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $cart_item ) {
		if ( $cart_item['product_id'] == $id && $cart_item['variation_id'] == $vid ) {
			WC()->cart->remove_cart_item( $cart_item['key'] );
		}
	}
	return true;
	die();
}

add_action( 'wp_ajax_remove_item_from_cart', 'remove_item_from_cart' );
add_action( 'wp_ajax_nopriv_remove_item_from_cart', 'remove_item_from_cart' );
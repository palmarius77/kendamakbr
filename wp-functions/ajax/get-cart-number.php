<?php
function get_cart_number() {
	global $woocommerce;
	echo $woocommerce->cart->cart_contents_count;
	die();
}

add_action( 'wp_ajax_get_cart_number', 'get_cart_number' );
add_action( 'wp_ajax_nopriv_get_cart_number', 'get_cart_number' );
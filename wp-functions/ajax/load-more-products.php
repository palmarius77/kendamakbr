<?php
if ( ! function_exists( 'kendamakbr_more_products_ajax' ) ) {
	function kendamakbr_more_products_ajax() {

		$prpp   = ( isset( $_POST['prpp'] ) ) ? $_POST['prpp'] : 12;
		$offset = ( isset( $_POST['offset'] ) ) ? $_POST['offset'] : 0;
		$paged  = ( isset( $_POST['paged'] ) ) ? $_POST['paged'] : 1;

		$product_cat = ( isset( $_POST['product_cat'] ) ) ? $_POST['product_cat'] : 0;
		$orderby     = ( isset( $_POST['orderby'] ) ) ? $_POST['orderby'] : '';
		$s           = ( isset( $_POST['s'] ) ) ? $_POST['s'] : '';
		$price_range = ( isset( $_POST['price_range'] ) ) ? $_POST['price_range'] : '';
		$color       = ( isset( $_POST['color'] ) ) ? $_POST['color'] : '';
		$brand       = ( isset( $_POST['brand'] ) ) ? $_POST['brand'] : '';
		$paint       = ( isset( $_POST['paint'] ) ) ? $_POST['paint'] : '';
		$wood        = ( isset( $_POST['wood'] ) ) ? $_POST['wood'] : '';

		$args = array(
				'post_type'      => 'product',
				'posts_per_page' => $prpp,
				'offset'         => $offset,
				'paged'          => $paged,
				'orderby'        => 'date',
				'order'          => 'DESC',
		);
		if ( $product_cat ) {
			$args['product_cat'] = $product_cat;
		}
		if ( $orderby ) {
			switch ( $orderby ) {
				case 'price':
					$args['orderby']  = 'meta_value_num';
					$args['order']    = 'ASC';
					$args['meta_key'] = '_price';
					break;
				case 'price-desc':
					$args['orderby']  = 'meta_value_num';
					$args['order']    = 'DESC';
					$args['meta_key'] = '_price';
					break;
				case 'rating':
					$args['orderby']  = 'meta_value_num';
					$args['order']    = 'DESC';
					$args['meta_key'] = '_wc_average_rating';
					break;
				case 'relevance':
					$args['orderby'] = 'relevance';
					$args['order']   = 'DESC';
					break;
				default:
					$args['orderby'] = 'date';
					$args['order']   = 'desc';
					break;
			}
		}
		if ( $s ) {
			$args['s'] = $s;
			if ( ! $orderby ) {
				$args['orderby'] = 'relevance';
				$args['order']   = 'DESC';
			}
		}
		if ( $price_range ) {
			$price_range          = explode( ";", $price_range );
			$from                 = $price_range[0];
			$to                   = $price_range[1];
			$args['meta_query'][] = array(
					'key'     => '_price',
					'value'   => array( $from, $to ),
					'compare' => 'BETWEEN',
					'type'    => 'NUMERIC',
			);
		}
		$args['tax_query']['relation'] = 'AND';
		if ( $color ) {
			$args['tax_query'][] = array(
					'taxonomy' => 'pa_color',
					'field'    => 'slug',
					'terms'    => $color,
					'operator' => 'IN',
			);
		}
		if ( $brand ) {
			$args['tax_query'][] = array(
					'taxonomy' => 'brand',
					'field'    => 'slug',
					'terms'    => $brand,
					'operator' => 'IN',
			);
		}
		if ( $paint ) {
			$args['tax_query'][] = array(
					'taxonomy' => 'paint',
					'field'    => 'slug',
					'terms'    => $paint,
					'operator' => 'IN',
			);
		}
		if ( $wood ) {
			$args['tax_query'][] = array(
					'taxonomy' => 'wood',
					'field'    => 'slug',
					'terms'    => $wood,
					'operator' => 'IN',
			);
		}

		$loop = new WP_Query( $args );

		if ( $loop->have_posts() ) :
			while ( $loop->have_posts() ) :
				$loop->the_post();
				productTeaser();
			endwhile;
		endif;
		wp_reset_postdata();
		die();
	}
}
add_action( 'wp_ajax_nopriv_kendamakbr_more_products_ajax', 'kendamakbr_more_products_ajax' );
add_action( 'wp_ajax_kendamakbr_more_products_ajax', 'kendamakbr_more_products_ajax' );
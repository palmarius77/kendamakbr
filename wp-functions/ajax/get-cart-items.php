<?php
function get_cart_items() {
	global $woocommerce, $cur_simb;
	?>
  <div class="dropdown-cart-wrapper">
    <div class="cart-wrapper">
			<?php
			$items = $woocommerce->cart->get_cart();
			foreach ( $items as $item => $values ) {
				$_product = wc_get_product( $values['data']->get_id() );
				$price    = $_product->price;
				?>
        <div class="product-row">
					<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $values['product_id'] ), 'gallery_small' ); ?>
          <a href="<?php echo get_permalink( $values['product_id'] ); ?>" class="image">
            <img src="<?php echo $image[0]; ?>" alt="thumbnail">
          </a>
          <span><a href="<?php echo get_permalink( $values['product_id'] ); ?>"><?php echo $values['data']->name; ?></a></span>
          <span>x<?php echo $values['quantity']; ?></span>
          <span class="price"><?php echo $cur_simb . $price; ?></span>
          <a href="#" data-id="<?php echo $values['product_id']; ?>" data-vid="<?php echo $values['data']->variation_id; ?>" class="delete-item"><i class="fas fa-times-circle"></i></a>
        </div>
			<?php } ?>
      <div class="floating-objects small-mt">
        <a href="<?php echo $woocommerce->cart->get_cart_url(); ?>" class="button small left"><?php _e( 'View cart', 'kendamakbr' ); ?></a>
        <a href="<?php echo $woocommerce->cart->get_checkout_url(); ?>"
           class="button small right dark"><?php _e( 'Complete order', 'kendamakbr' ); ?></a>
      </div>
    </div>
  </div>
	<?php
  die();
}

add_action( 'wp_ajax_get_cart_items', 'get_cart_items' );
add_action( 'wp_ajax_nopriv_get_cart_items', 'get_cart_items' );
<?php
if ( ! function_exists( 'kendamakbr_count_posts' ) ) {
	function kendamakbr_count_posts() {

		$cat = ( isset( $_POST['cat'] ) ) ? $_POST['cat'] : 0;
		$tag = ( isset( $_POST['tag'] ) ) ? $_POST['tag'] : '';
		$s   = ( isset( $_POST['s'] ) ) ? $_POST['s'] : '';
		if ( $cat ) {
			$category = get_category( $cat );
			echo $category->category_count;
		} elseif ( $tag ) {
			$tag = get_term_by( 'name', $tag, 'post_tag' );
			echo $tag->count;
		} elseif ( $s ) {
			$args  = array(
					'posts_per_page' => - 1,
					's'              => $s
			);
			$query = new WP_Query( $args );
			echo $query->post_count;
		} else {
			$count_posts = wp_count_posts( 'post' );
			$posts       = $count_posts->publish;
			echo $posts;
		}
		die();
	}
}
add_action( 'wp_ajax_nopriv_kendamakbr_count_posts', 'kendamakbr_count_posts' );
add_action( 'wp_ajax_kendamakbr_count_posts', 'kendamakbr_count_posts' );
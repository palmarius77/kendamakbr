<?php
if ( ! function_exists( 'kendamakbr_more_post_ajax' ) ) {
	function kendamakbr_more_post_ajax() {

		$ppp       = ( isset( $_POST['ppp'] ) ) ? $_POST['ppp'] : 6;
		$s         = ( isset( $_POST['s'] ) ) ? $_POST['s'] : null;
		$post_type = ( isset( $_POST['post_type'] ) ) ? $_POST['post_type'] : 'post';
		$cat       = ( isset( $_POST['cat'] ) ) ? $_POST['cat'] : 0;
		$tag       = ( isset( $_POST['tag'] ) ) ? $_POST['tag'] : '';
		$offset    = ( isset( $_POST['offset'] ) ) ? $_POST['offset'] : 0;
		$paged     = ( isset( $_POST['paged'] ) ) ? $_POST['paged'] : 1;

		$args = array(
				'posts_per_page' => $ppp,
				'cat'            => $cat,
				'tag'            => $tag,
				'offset'         => $offset,
				'paged'          => $paged,
		);

		if ( $cat ) {
			$args['cat'] = $cat;
		}
		if ( $tag ) {
			$args['tag'] = $tag;
		}
		if ( $s ) {
			$args['s'] = $s;
		}
		if ( $post_type != 'all' ) {
			$args['post_type'] = $post_type;
		}

		$loop = new WP_Query( $args );

		if ( $loop->have_posts() ) :
			while ( $loop->have_posts() ) :
				$loop->the_post();
				postTeaser();
			endwhile;
		endif;
		wp_reset_postdata();
		die();
	}
}
add_action( 'wp_ajax_nopriv_kendamakbr_more_post_ajax', 'kendamakbr_more_post_ajax' );
add_action( 'wp_ajax_kendamakbr_more_post_ajax', 'kendamakbr_more_post_ajax' );
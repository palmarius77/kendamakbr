<?php
function custom_excerpt_length( $length ) {
	return 23;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function get_excerpt( $limit ) {
	if ( has_excerpt() ) {
		$excerpt = get_the_excerpt();
	} else {
		$excerpt = get_the_content();
	}
	$excerpt = preg_replace( ' (\[.*?\])', '', $excerpt );
	$excerpt = strip_shortcodes( $excerpt );
	$excerpt = strip_tags( $excerpt );
	$excerpt = substr( $excerpt, 0, $limit );
	$excerpt = substr( $excerpt, 0, strripos( $excerpt, ' ' ) );
	$excerpt = trim( preg_replace( '/\s+/', ' ', $excerpt ) );
	$excerpt = $excerpt . '. <a href=' . get_permalink() . '>' . __( 'Read more...', 'kendamakbr' ) . '</a>';

	return $excerpt;
}



<?php

add_action('wp_enqueue_scripts', function () {
  if (is_admin()) {
    return;
  }

  global $td_uri;
  wp_deregister_script('jquery');

  wp_register_script('jquery', '//code.jquery.com/jquery-2.2.4.min.js', FALSE, '2.2.4');
  wp_register_script('jquery-ui', '//code.jquery.com/ui/1.12.1/jquery-ui.min.js', FALSE, '1.12.1', TRUE);
  wp_register_script('fancybox', $td_uri . '/js/jquery.fancybox.min.js', FALSE, '3.2.10', TRUE);
  wp_register_script('slick', $td_uri . '/slick/slick.min.js', FALSE, '1.6.0', TRUE);
  wp_register_script('selectric', $td_uri . '/js/jquery.selectric.min.js', FALSE, '1.11.1', TRUE);
  wp_register_script('jquery-visible', $td_uri . '/js/jquery.visible.min.js', FALSE, '1', TRUE);
  wp_register_script('aos', $td_uri . '/js/aos.js', FALSE, '1', TRUE);
  wp_register_script('range-slider', $td_uri . '/js/ion.rangeSlider.min.js', FALSE, '2.2.0', TRUE);
  wp_register_script('elevate-zoom', $td_uri . '/js/jquery.elevateZoom-3.0.8.min.js', FALSE, '3.0.8', TRUE);
  wp_register_script('script', $td_uri . '/js/script.js', FALSE, '1', TRUE);

  wp_enqueue_script('jquery');
  wp_enqueue_script('jquery-ui');
  wp_enqueue_script('fancybox');
  wp_enqueue_script('slick');
  wp_enqueue_script('selectric');
  wp_enqueue_script('jquery-visible');
  wp_enqueue_script('aos');
  wp_enqueue_script('range-slider');
  wp_enqueue_script('elevate-zoom');
  wp_enqueue_script('script');

  wp_enqueue_style('g-fonts', 'https://fonts.googleapis.com/css?family=Teko:400,500,700|Titillium+Web:300,300i,400,400i,600,600i,700,700i');
  wp_enqueue_style('fonts', $td_uri . '/css/fonts.css');
  wp_enqueue_style('font-awesome', $td_uri . '/css/fontawesome-all.min.css');
  wp_enqueue_style('aos', $td_uri . '/css/aos.css');
  wp_enqueue_style('slick', $td_uri . '/slick/slick.css');
  wp_enqueue_style('slick-theme', $td_uri . '/slick/slick-theme.css');
  wp_enqueue_style('selectric', $td_uri . '/css/selectric.css');
  wp_enqueue_style('fancybox', $td_uri . '/css/jquery.fancybox.min.css');
  wp_enqueue_style('range-slider', $td_uri . '/css/ion.rangeSlider.css');
  wp_enqueue_style('range-skin', $td_uri . '/css/ion.rangeSlider.skinNice.css');
  wp_enqueue_style('stylesheet', $td_uri . '/style.css');
}, 11);

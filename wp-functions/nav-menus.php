<?php

add_action('init', function () {
  register_nav_menus([
    'header' => __('Header menu'),
    'header_logged_in' => __('Header menu logged in'),
    'header_mobile'           => __( 'Header menu mobile' ),
    'header_mobile_logged_in' => __( 'Header menu mobile logged in' ),
    'footer' => __('Footer menu'),
  ]);
});

<?php
add_action( 'wp_head', 'kbr_ajaxurl' );
function kbr_ajaxurl() {
	?>
  <script type="text/javascript">
    var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
    var is_user_logged_in = '<?php echo is_user_logged_in(); ?>';
  </script>
	<?php
}
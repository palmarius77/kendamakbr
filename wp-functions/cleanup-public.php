<?php

/**
 * Remove meta information from page head
 */

add_action( 'init', function () {
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'index_rel_link' );
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'rest_output_link_wp_head' );
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
	remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
	remove_action( 'wp_head', 'wp_resource_hints', 2 );
} );

add_action( 'after_setup_theme', function () {
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	remove_action( 'wp_head', 'feed_links', 2 );
} );

/**
 * Disable WP_Embed
 */
add_action( 'wp_footer', function () {
	wp_deregister_script( 'wp-embed' );
} );

/**
 * Excerpts
 */
add_filter( 'excerpt_more', function ( $more ) {
	return '';
} );

add_filter( 'excerpt_length', function ( $length ) {
	return 10;
}, 999 );

/**
 * Disable version information from showing in the browser
 */
add_filter( 'the_generator', function () {
	return '';
} );

/**
 * Remove version information from all JavaScript and Stylesheet requests
 * "?ver=1.1"       => ""
 * "?test&ver=1.1"  => "?test"
 */
function hando_wp_version_strings_remove( $src ) {
	if ( is_admin() ) {
		return $src;
	}

	return preg_replace( '/[&\?]?ver=[\d\.]+/mis', '', $src, 1 );
}

add_filter( 'script_loader_src', 'hando_wp_version_strings_remove' );
add_filter( 'style_loader_src', 'hando_wp_version_strings_remove' );
<?php
add_filter( 'registration_errors', 'extra_fields_registration_errors', 10, 3 );
function extra_fields_registration_errors( $errors, $sanitized_user_login, $user_email ) {

	if ( empty( $_POST['first_name'] ) || ! empty( $_POST['first_name'] ) && trim( $_POST['first_name'] ) == '' ) {
		$errors->add( 'first_name_error', __( '<strong>ERROR</strong>: You must include a first name.', 'kendamakbr' ) );
	}
	if ( empty( $_POST['last_name'] ) || ! empty( $_POST['last_name'] ) && trim( $_POST['first_name'] ) == '' ) {
		$errors->add( 'last_name_error', __( '<strong>ERROR</strong>: You must include a first name.', 'kendamakbr' ) );
	}
	if ( empty( $_POST['password'] ) || ! empty( $_POST['password'] ) && trim( $_POST['password'] ) == '' ) {
		$errors->add( 'password_error', __( '<strong>ERROR</strong>: Please enter a password.', 'kendamakbr' ) );
	}
	if ( empty( $_POST['password2'] ) || ! empty( $_POST['password2'] ) && trim( $_POST['password2'] ) == '' ) {
		$errors->add( 'password_error', __( '<strong>ERROR</strong>: Please enter a password.', 'kendamakbr' ) );
	}
	if ( $_POST['password'] != $_POST['password2'] ) {
		$errors->add( 'password_confirm_error', __( '<strong>ERROR</strong>: Password must be the same.', 'kendamakbr' ) );
	}

	return $errors;
}

add_action( 'user_register', 'extra_fields_user_register' );
function extra_fields_user_register( $user_id ) {
	if ( ! empty( $_POST['first_name'] ) ) {
		update_user_meta( $user_id, 'first_name', trim( $_POST['first_name'] ) );
		update_user_meta( $user_id, 'last_name', trim( $_POST['last_name'] ) );
		wp_update_user( array(
				'ID'           => $user_id,
				'display_name' => trim( $_POST['first_name'] ) . " " . trim( $_POST['last_name'] )
		) );
	}
	if ( ! empty( $_POST['password'] ) ) {
		wp_set_password( trim( $_POST['password'] ), $user_id );
	}
}
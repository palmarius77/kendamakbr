<?php
$wdm_address_fields = array(
		'address_1',
		'street_nb',
		'address_2',
		'postcode',
		'country',
		'city',
		'state',
);
$wdm_ext_fields     = array(
		'street_nb'
);

// reorder checkout fields
add_filter( "woocommerce_checkout_fields", "custom_override_checkout_fields", 1 );
function custom_override_checkout_fields( $fields ) {
	$fields['billing']['billing_first_name']['priority'] = 1;
	$fields['billing']['billing_last_name']['priority']  = 2;
	$fields['billing']['billing_email']['priority']      = 3;
	$fields['billing']['billing_phone']['priority']      = 4;
	$fields['billing']['billing_address_1']['priority']  = 5;
	$fields['billing']['billing_street_nb']['priority']  = 6;
	$fields['billing']['billing_address_2']['priority']  = 7;
	$fields['billing']['billing_postcode']['priority']   = 8;
	$fields['billing']['billing_country']['priority']    = 9;
	$fields['billing']['billing_state']['priority']      = 10;
	$fields['billing']['billing_city']['priority']       = 11;
	unset( $fields['billing']['billing_company'] );

	$fields['billing']['billing_first_name']['class']   = [ 'form-row-first' ];
	$fields['billing']['billing_last_name']['class']    = [ 'form-row-last' ];
	$fields['billing']['billing_phone']['class']        = [ 'form-row-first' ];
	$fields['billing']['billing_email']['class']        = [ 'form-row-last' ];
	$fields['shipping']['shipping_first_name']['class'] = [ 'form-row-first' ];
	$fields['shipping']['shipping_last_name']['class']  = [ 'form-row-last' ];

	$fields['billing']['billing_address_1']['label']         = __( 'Street Name', 'kendamakbr' );
	$fields['billing']['billing_address_1']['placeholder']   = __( 'Street Name', 'kendamakbr' );
	$fields['shipping']['shipping_address_1']['label']       = __( 'Street Name', 'kendamakbr' );
	$fields['shipping']['shipping_address_1']['placeholder'] = __( 'Street Name', 'kendamakbr' );

	$fields['shipping']['shipping_company']['priority']    = 1;
	$fields['shipping']['shipping_first_name']['priority'] = 2;
	$fields['shipping']['shipping_last_name']['priority']  = 3;
	$fields['shipping']['shipping_address_1']['priority']  = 5;
	$fields['shipping']['shipping_street_nb']['priority']  = 6;
	$fields['shipping']['shipping_address_2']['priority']  = 7;
	$fields['shipping']['shipping_postcode']['priority']   = 8;
	$fields['shipping']['shipping_country']['priority']    = 9;
	$fields['shipping']['shipping_state']['priority']      = 10;
	$fields['shipping']['shipping_city']['priority']       = 11;

	$fields['billing']['billing_state']['required']   = __( true );
	$fields['shipping']['shipping_state']['required'] = __( true );

	$fields['billing']['billing_first_name']['label']       = __( 'First Name', 'kendamakbr' );
	$fields['billing']['billing_first_name']['placeholder'] = __( 'First Name', 'kendamakbr' );
	$fields['billing']['billing_first_name']['required']    = __( true );
	$fields['billing']['billing_last_name']['label']        = __( 'Last Name', 'kendamakbr' );
	$fields['billing']['billing_last_name']['placeholder']  = __( 'Last Name', 'kendamakbr' );
	$fields['billing']['billing_last_name']['required']     = __( true );

	$fields['shipping']['shipping_company']['label']          = __( 'Company Name', 'kendamakbr' );
	$fields['shipping']['shipping_company']['placeholder']    = __( 'Company Name', 'kendamakbr' );
	$fields['shipping']['shipping_company']['required']       = __( true );
	$fields['shipping']['shipping_first_name']['label']       = __( 'First Name', 'kendamakbr' );
	$fields['shipping']['shipping_first_name']['placeholder'] = __( 'First Name', 'kendamakbr' );
	$fields['shipping']['shipping_first_name']['required']    = __( true );
	$fields['shipping']['shipping_last_name']['label']        = __( 'Last Name', 'kendamakbr' );
	$fields['shipping']['shipping_last_name']['placeholder']  = __( 'Last Name', 'kendamakbr' );
	$fields['shipping']['shipping_last_name']['required']     = __( true );
	$fields['shipping']['shipping_country']['label']          = __( 'Country', 'kendamakbr' );
	$fields['shipping']['shipping_country']['placeholder']    = __( 'Country', 'kendamakbr' );
	$fields['shipping']['shipping_country']['required']       = __( true );
	$fields['billing']['billing_country']['label']            = __( 'Country', 'kendamakbr' );
	$fields['billing']['billing_country']['placeholder']      = __( 'Country', 'kendamakbr' );
	$fields['billing']['billing_country']['required']         = __( true );

	$fields['billing']['billing_address_1']['placeholder']   = __( 'Street Name', 'kendamakbr' );
	$fields['shipping']['shipping_address_1']['placeholder'] = __( 'Street Name', 'kendamakbr' );

	$fields['billing']['billing_street_nb']['label']   = __( 'Street Number', 'kendamakbr' );
	$fields['shipping']['shipping_street_nb']['label'] = __( 'Street Number', 'kendamakbr' );

	$fields['billing']['billing_street_nb']['placeholder']   = __( 'Ex: 12', 'kendamakbr' );
	$fields['shipping']['shipping_street_nb']['placeholder'] = __( 'Ex: 12', 'kendamakbr' );

	$countries_obj                                     = new WC_Countries();
	$countries                                         = $countries_obj->__get( 'countries' );
	$fields['billing']['billing_country']['type']      = 'select';
	$fields['billing']['billing_country']['clear']     = false;
	$fields['billing']['billing_country']['options']   = $countries;
	$fields['shipping']['shipping_country']['type']    = 'select';
	$fields['shipping']['shipping_country']['clear']   = false;
	$fields['shipping']['shipping_country']['options'] = $countries;

	$fields['billing']['billing_state']['label']    = __( 'State / County', 'kendamakbr' );
	$fields['billing']['billing_state']['type']     = 'text';
	$fields['billing']['billing_state']['required'] = true;

	$fields['shipping']['shipping_state']['label']    = __( 'State / County', 'kendamakbr' );
	$fields['shipping']['shipping_state']['type']     = 'text';
	$fields['shipping']['shipping_state']['required'] = true;

	$fields['shipping']['vatno'] = array(
			'label'    => __( 'VAT Number', 'woocommerce' ),
			'required' => false,
			'class'    => array( 'form-row-wide' ),
			'type'     => 'text',
			'priority' => 4
	);

	return $fields;
}

add_filter( 'woocommerce_default_address_fields', 'custom_override_default_locale_fields' );
function custom_override_default_locale_fields( $fields ) {
	//pre_print_r($fields);
	$temp_fields                     = array();
	$fields['address_1']['priority'] = 5;
	$fields['street_nb']             = array(
			'label'    => __( 'Street Number', 'woocommerce' ),
			'required' => true,
			'class'    => array( 'form-row-wide' ),
			'type'     => 'text',
			'priority' => 6
	);
	$fields['address_2']['priority'] = 7;
	$fields['postcode']['priority']  = 8;
	$fields['country']['priority']   = 9;
	$fields['state']['priority']     = 10;
	$fields['city']['priority']      = 11;
	$fields['address_1']['label']    = __( 'Street Name', 'kendamakbr' );

	$fields['address_1']['class'] = [ 'field-row-70' ];
	$fields['street_nb']['class'] = [ 'field-row-30' ];

	//$fields['billing_country']['type'] = 'select';

	$fields['shipping_state']['label']    = __( 'State / County', 'kendamakbr' );
	$fields['shipping_state']['type']     = 'text';
	$fields['shipping_state']['required'] = true;
	$fields['state']['label']             = __( 'State / County', 'kendamakbr' );
	$fields['state']['type']              = 'text';
	$fields['state']['required']          = true;

	global $wdm_address_fields;
	foreach ( $wdm_address_fields as $fky ) {
		$temp_fields[ $fky ] = $fields[ $fky ];
	}
	$fields = $temp_fields;

	//pre_print_r($fields); die();
	return $fields;
}

add_filter( 'woocommerce_formatted_address_replacements', 'wdm_formatted_address_replacements', 99, 2 );
function wdm_formatted_address_replacements( $address, $args ) {
	$address['{address_1}'] = $args['address_1'] . ", " . $args['street_nb'];

	return $address;
}

add_filter( 'woocommerce_order_formatted_billing_address', 'wdm_update_formatted_billing_address', 99, 2 );
function wdm_update_formatted_billing_address( $address, $obj ) {
	global $wdm_address_fields;
	if ( is_array( $wdm_address_fields ) ) {
		foreach ( $wdm_address_fields as $waf ) {
			$address[ $waf ] = $obj->{'billing_' . $waf};
		}
	}

	return $address;
}

add_filter( 'woocommerce_order_formatted_shipping_address', 'wdm_update_formatted_shipping_address', 99, 2 );
function wdm_update_formatted_shipping_address( $address, $obj ) {
	global $wdm_address_fields;
	if ( is_array( $wdm_address_fields ) ) {
		foreach ( $wdm_address_fields as $waf ) {
			$address[ $waf ] = $obj->{'shipping_' . $waf};
		}
	}

	return $address;
}

add_filter( 'woocommerce_my_account_my_address_formatted_address', 'wdm_my_account_address_formatted_address', 99, 3 );
function wdm_my_account_address_formatted_address( $address, $customer_id, $name ) {
	global $wdm_address_fields;
	if ( is_array( $wdm_address_fields ) ) {
		foreach ( $wdm_address_fields as $waf ) {
			$address[ $waf ] = get_user_meta( $customer_id, $name . '_' . $waf, true );
		}
	}

	return $address;
}

add_filter( 'woocommerce_admin_billing_fields', 'wdm_add_extra_customer_field' );
add_filter( 'woocommerce_admin_shipping_fields', 'wdm_add_extra_customer_field' );

function wdm_add_extra_customer_field( $fields ) {

	$fields = wdm_override_default_address_fields( $fields );
	global $wdm_ext_fields;
	if ( is_array( $wdm_ext_fields ) ) {
		foreach ( $wdm_ext_fields as $wef ) {
			$fields[ $wef ]['show'] = false; //hide the way they are display by default as we have now merged them within the address field
		}
	}

	return $fields;
}

function wdm_override_default_address_fields( $address_fields ) {
	$temp_fields                 = array();
	$address_fields['street_nb'] = array(
			'label'    => __( 'Str. Number', 'woocommerce' ),
			'required' => true,
			'class'    => array( 'form-row-wide' ),
			'type'     => 'text',
			'priority' => 6
	);

	global $wdm_address_fields;
	foreach ( $wdm_address_fields as $fky ) {
		$temp_fields[ $fky ] = $address_fields[ $fky ];
	}
	$address_fields = $temp_fields;

	return $address_fields;
}

function array_sort( $array, $on, $order = SORT_ASC ) {
	$new_array      = array();
	$sortable_array = array();

	if ( count( $array ) > 0 ) {
		foreach ( $array as $k => $v ) {
			if ( is_array( $v ) ) {
				foreach ( $v as $k2 => $v2 ) {
					if ( $k2 == $on ) {
						$sortable_array[ $k ] = $v2;
					}
				}
			} else {
				$sortable_array[ $k ] = $v;
			}
		}

		switch ( $order ) {
			case SORT_ASC:
				asort( $sortable_array );
				break;
			case SORT_DESC:
				arsort( $sortable_array );
				break;
		}

		foreach ( $sortable_array as $k => $v ) {
			$new_array[ $k ] = $array[ $k ];
		}
	}

	return $new_array;
}

function wc_billing_field_strings( $translated_text, $text, $domain ) {
	switch ( $translated_text ) {
		case 'Billing details' :
			$translated_text = __( 'Shipping details', 'woocommerce' );
			break;
	}

	return $translated_text;
}

add_filter( 'gettext', 'wc_billing_field_strings', 20, 3 );

// Save Field Into User Meta
add_action( 'woocommerce_checkout_update_user_meta', 'bbloomer_checkout_field_update_user_meta' );
function bbloomer_checkout_field_update_user_meta( $user_id ) {
	if ( $user_id && $_POST['vatno'] ) update_user_meta( $user_id, 'vatno', sanitize_text_field( $_POST['vatno'] ) );
}

// Display User Field @ User Profile
add_action( 'show_user_profile', 'bbloomer_show_user_extra_field' );
add_action( 'edit_user_profile', 'bbloomer_show_user_extra_field' );
function bbloomer_show_user_extra_field( $user ) { ?>
  <h3>Additional Fields</h3>
  <table class="form-table">
  <tr>
    <th><label for="vatno">VAT Number</label></th>
    <td><input type="text" name="vatno" value="<?php echo esc_attr( get_the_author_meta( 'vatno', $user->ID ) ); ?>" class="regular-text"/>
    </td>
  </tr>
  </table><?php
}

// Save User Field When Changed From the Profile Page
add_action( 'personal_options_update', 'bbloomer_save_extra_fields' );
add_action( 'edit_user_profile_update', 'bbloomer_save_extra_fields' );
function bbloomer_save_extra_fields( $user_id ) {
	update_user_meta( $user_id, 'vatno', sanitize_text_field( $_POST['vatno'] ) );
}

// Update order meta with field value
add_action( 'woocommerce_checkout_update_order_meta', 'bbloomer_custom_checkout_field_update_user_meta' );
function bbloomer_custom_checkout_field_update_user_meta( $order_id ) {
	if ( $_POST['vatno'] ) update_post_meta( $order_id, 'vatno', sanitize_text_field( $_POST['vatno'] ) );
}

// Display User Field @ Order Meta
add_action( 'woocommerce_admin_order_data_after_billing_address', 'bbloomer_checkout_field_display_admin_order_meta', 10, 1 );
function bbloomer_checkout_field_display_admin_order_meta( $order ) {
	echo '<p><strong>' . __( 'VAT Number' ) . ':</strong> ' . get_post_meta( $order->id, 'vatno', true ) . '</p>';
}
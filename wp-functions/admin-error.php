<?php
function show_admin_error( $type ) {
	$current_user = wp_get_current_user();
	if ( user_can( $current_user, 'administrator' ) ) {
		switch ( $type ) {
			case 'missing_image':
				echo '<div class="admin_red_msg">' . __( 'Not all main images are selected', 'kendamakbr' ) . ': ';
				edit_post_link( 'edit product', '', '' );
				echo ' </div>';
				break;
		}
	}
}
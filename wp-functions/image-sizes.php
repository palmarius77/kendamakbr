<?php

/**
 * Register image sizes
 */
add_theme_support( 'post-thumbnails' );
add_filter( 'jpeg_quality', function ( $arg ) {
	return 100;
} );

if ( ! get_option( 'init_media_sizes' ) ) {
	update_option( 'init_media_sizes', true );

	update_option( 'thumbnail_size_w', 230 );
	update_option( 'thumbnail_size_h', 230 );
	update_option( 'thumbnail_crop', 1 );

	update_option( 'medium_size_w', 650 );
	update_option( 'medium_size_h', 650 );

	update_option( 'large_size_w', 1440 );
	update_option( 'large_size_h', 805 );
}

add_image_size( 'full_hd', 1920, 1080, true );
add_image_size( 'full_mob', 720, 1036, true );
add_image_size( 'landscape', 1920, 300, true );
add_image_size( 'half_screen', 690, 450, false );

add_image_size( 'prod_thumb_med', 425, 490, true );
add_image_size( 'prod_thumb_wide', 840, 450, true );

add_image_size( 'gallery_med', 410, 410, true );
add_image_size( 'gallery_small', 80, 80, true );

add_image_size( 'blog_thumb', 705, 468, true );
add_image_size( 'blog_thumb_2', 705, 359, true );

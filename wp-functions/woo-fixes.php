<?php
add_theme_support( 'woocommerce' );
// disable ship to different address default
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_false' );

// remove woocommerce breadcrumb
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

// remove sale flash -> added again after
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10, 0 );

// remove hooks from woocommerce_single_product_summary -> attache them after in a different order
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 60 );

// remove hooks from woocommerce_after_single_product_summary
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

// move comment rating
remove_action( 'woocommerce_review_before_comment_meta', 'woocommerce_review_display_rating', 10 );
add_action( 'woocommerce_review_before_comment_text', 'woocommerce_review_display_rating', 10 );

// remove product teaser hooks
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );

// remove hooks from product archive
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );

// reorder hooks checkout page
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );
add_action( 'woocommerce_after_checkout_form', 'woocommerce_checkout_login_form', 10 );

// display an 'Out of Stock' label on archive pages
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_no_stock', 10 );
function woocommerce_template_loop_no_stock() {
	global $product;
	if ( ! $product->managing_stock() && ! $product->is_in_stock() ) {
		echo '<div class="onsale no_stock"><span>' . __( 'no stock', 'kendamakbr' ) . '</span></div>';
	}
}

// update price html
add_filter( 'woocommerce_get_price_html', 'kbr_price_html', 100, 2 );
function kbr_price_html( $price, $product ) {
	$price = str_replace( '<del>', '<span class="old-price">', $price );
	$price = str_replace( '</del>', '</span><span> - </span>', $price );
	if ( $product->is_on_sale() ) {
		$price = str_replace( '<ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">', '<span class="new-price"><span class="woocommerce-Price-amount amount new-price"><span class="woocommerce-Price-currencySymbol new-price">', $price );
		$price = str_replace( '</ins>', '</span>', $price );
	} else {
		$price = str_replace( '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">', '<span class="woocommerce-Price-amount amount new-price"><span class="woocommerce-Price-currencySymbol new-price">', $price );
	}

	return $price;
}

add_filter( 'woocommerce_template_loop_price', 'kbr_loop_price_html', 100, 2 );
function kbr_loop_price_html( $price, $product ) {
	$price = str_replace( '<del>', '<span class="old-price">', $price );
	$price = str_replace( '</del>', '</span><span> - </span>', $price );
	if ( $product->is_on_sale() ) {
		$price = str_replace( '<ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">', '<span class="new-price"><span class="woocommerce-Price-amount amount new-price"><span class="woocommerce-Price-currencySymbol new-price">', $price );
		$price = str_replace( '</ins>', '</span>', $price );
	} else {
		$price = str_replace( '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">', '<span class="woocommerce-Price-amount amount new-price"><span class="woocommerce-Price-currencySymbol new-price">', $price );
	}

	return $price;
}

function products_loop_filter( $query ) {
	if ( ! is_admin() && $query->is_main_query() ) {
		if ( $query->query_vars['post_type'] == 'product' ) {
			if ( ! isset( $_GET['orderby'] ) ) {
				$query->query_vars['orderby'] = 'date';
				$query->query_vars['order']   = 'DESC';
			}
			if ( isset( $_GET['price_range'] ) && $_GET['price_range'] != '' ) {
				$price_range = explode( ";", $_GET['price_range'] );
				$from        = $price_range[0];
				$to          = $price_range[1];
				$meta_q      = $query->query_vars['meta_query'];
				$meta_q[]    = array(
						array(
								'key'     => '_price',
								'value'   => array( $from, $to ),
								'compare' => 'BETWEEN',
								'type'    => 'NUMERIC',
						),
				);
				$query->set( 'meta_query', $meta_q );
			}
			if ( isset( $_GET['color'] ) && $_GET['color'] != '' ) {
				$tax_q   = $query->query_vars['tax_query'];
				$tax_q[] = array(
						'taxonomy' => 'pa_color',
						'field'    => 'slug',
						'terms'    => $_GET['color'],
						'operator' => 'IN',
				);
				$query->set( 'tax_query', $tax_q );
			}
		}
	}
}

add_action( 'pre_get_posts', 'products_loop_filter' );

// disable sort by popularity
function my_woocommerce_catalog_orderby( $orderby ) {
	unset( $orderby["relevance"] );
	unset( $orderby["popularity"] );

	return $orderby;
}

add_filter( "woocommerce_catalog_orderby", "my_woocommerce_catalog_orderby", 20 );


/**
 * Change number of related products output
 */
function woo_related_products_limit() {
	global $product;
	$args['posts_per_page'] = 8;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
function jk_related_products_args( $args ) {
	$args['posts_per_page'] = 8;
	$args['columns'] = 1;
	return $args;
}
<?php

add_action( 'wp_dashboard_setup', function () {
	global $wp_meta_boxes;
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments'] );
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary'] );
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary'] );
} );

add_filter( 'manage_edit-post_columns', function ( $columns ) {
	// unset($columns['author']); // Removes author
	// unset( $columns['comments'] ); // Removes comments
	// unset($columns['categories']); // Removes categories
	// unset($columns['tags']); // Removes tags
	return $columns;
}, 10, 1 );

add_action( 'admin_menu', function () {
	// remove_menu_page('edit.php'); // Dashboard > Posts
	// remove_menu_page('upload.php'); // Dashboard > Media
	// remove_menu_page('edit.php?post_type=page'); // Dashboard > Pages
	// remove_menu_page( 'edit-comments.php' ); // Dashboard > Comments
	// remove_menu_page('themes.php'); // Dashboard > Appearance
	// remove_menu_page('plugins.php'); // Dashboard > Plugins
	// remove_menu_page('users.php'); // Dashboard > Users
	// remove_menu_page('tools.php'); // Dashboard > Tools
	// remove_menu_page('options-general.php'); // Dashboard > Settings
	// remove_menu_page('link-manager.php'); // Dashboard > Links (removed from WP3+)
} );


add_action( 'admin_menu', function () {
	// remove_submenu_page( 'index.php', 'update-core.php' ); // Home & Updates
	// remove_submenu_page('edit.php', 'post-new.php'); // Posts > All posts / Add new
	// remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category'); // Posts > Category
	// remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag'); // Posts > Tags
	// remove_submenu_page('upload.php', 'media-new.php'); // Media > Library & Add new
	// remove_submenu_page('themes.php', 'themes.php'); // Appearance > Themes
	// remove_submenu_page('themes.php', 'nav-menus.php'); // Appearance >
	// remove_submenu_page('themes.php', 'widgets.php'); // Appearance >
	// remove_submenu_page('themes.php', 'customize.php'); // Appearance > Customize
	// remove_submenu_page('plugins.php', 'plugin-editor.php'); // Plugins > Editor
	// remove_submenu_page('plugins.php', 'plugin-install.php'); // Plugins > Insatll
	// remove_submenu_page('users.php', 'users.php'); // Users > Users
	// remove_submenu_page('users.php', 'user-new.php'); // Users > Add new
	// remove_submenu_page('options-general.php', 'options-writing.php'); // Settings > Writing
	// remove_submenu_page('options-general.php', 'options-reading.php'); // Settings > Reading
	// remove_submenu_page('options-general.php', 'options-discussion.php'); // Settings > Discussion
	// remove_submenu_page('options-general.php', 'options-media.php'); // Settings > Media
	// remove_submenu_page('options-general.php', 'options-permalink.php'); // Settings > Permalinks
	// remove_submenu_page('options-general.php', 'options-privacy.php'); // Settings > Privacy (removed from WP3+)
} );

add_action( 'admin_menu', function () {
	remove_action( 'admin_notices', 'update_nag', 3 );
} );

// Hide admin bar for non admin users
global $current_user; // Use global
wp_get_current_user(); // Make sure global is set, if not set it.
if ( ! user_can( $current_user, "manage_options" ) ) {
	show_admin_bar( false );
}

// hide content editor for certain pages
add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];
	if ( ! isset( $post_id ) ) return;
	$template_file = get_post_meta( $post_id, '_wp_page_template', true );
	if ( $template_file == 'page-team.php' ) {
		remove_post_type_support( 'page', 'editor' );
	}
}

function admin_scripts( $hook ) {
	if ( 'post-new.php' == $hook ) {
		wp_enqueue_script( 'post-new-scripts', get_template_directory_uri() . '/js/admin/post-new-scripts.js' );
	}
}
add_action( 'admin_enqueue_scripts', 'admin_scripts' );
<?php
/*
Template Name: Team
*/
?>
<?php get_header();
wp_reset_query(); ?>

  <div class="about-page inner-page">

		<?php require_once 'wp-partials/top-banner.php';
		$video_thumbnail = get_field( 'video_thumbnail' );
		$video_url       = get_field( 'video_url' );
		$text_1          = get_field( 'text_1' );
		$right_image     = get_field( 'right_image' );
		$text_2          = get_field( 'text_2' );
		?>

    <div class="page-wrapper default-page">
      <div class="container">

				<?php require_once 'wp-partials/breadcrumb.php'; ?>

        <div class="page-content">

          <div class="row big-gutter small-mb">
						<?php if ( $video_thumbnail ): ?>
            <div class="col_50">
              <a href="<?php echo $video_url; ?>" class="video-frame fancybox">
                <img src="<?php echo $video_thumbnail['sizes']['half_screen']; ?>" alt="<?php echo $video_thumbnail['alt']; ?>"><i
                    class="fas fa-play-circle"></i>
              </a>
            </div>
            <div class="col_50">
							<?php endif;
							echo $text_1;
							if ( $video_thumbnail ): ?>
            </div>
					<?php endif; ?>
          </div>
          <div class="row big-gutter small-mb">
						<?php if ( $right_image ): ?>
            <div class="col_50">
							<?php endif;
							echo $text_2;
							if ( $right_image ): ?>
            </div>
            <div class="col_50">
              <img src="<?php echo $right_image['sizes']['half_screen']; ?>" alt="<?php echo $right_image['alt']; ?>">
            </div>
					<?php endif; ?>
          </div>

        </div><!-- End .page-content -->

      </div>
    </div>

  </div>

<?php get_footer(); ?>
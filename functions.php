<?php
// This is required for $_SESSION variables to work
if ( ! session_id() ) {
	session_start();
}

function pre_print_r( $it ) {
	echo '<pre>';
	print_r( $it );
	echo '</pre>';
}

//loading theme text domain (language locatlization)
load_theme_textdomain( 'f', get_template_directory() . '/languages' );

// globals
global $td_uri, $cur, $cur_simb;
$td_uri   = get_template_directory_uri();
$cur      = get_option( 'woocommerce_currency' );
$cur_simb = get_woocommerce_currency_symbol( $cur );

/**
 * Custom post types
 */
//require_once 'wp-types/...';

/**
 * Custom taxonomies
 */
require_once 'wp-taxonomies/product-brand.php';
require_once 'wp-taxonomies/product-paint.php';
require_once 'wp-taxonomies/product-wood.php';

/**
 * Ajax functions
 */
require_once 'wp-functions/ajax/remove-from-cart.php';
require_once 'wp-functions/ajax/get-cart-number.php';
require_once 'wp-functions/ajax/get-cart-items.php';
require_once 'wp-functions/ajax/load-more-posts.php';
require_once 'wp-functions/ajax/count-posts.php';
require_once 'wp-functions/ajax/add-to-cart-single.php';
require_once 'wp-functions/ajax/load-more-products.php';

/**
 * Custom fields
 */
require_once 'wp-fields/options.php';

/**
 * Clean-up functions
 */
require_once 'wp-functions/cleanup-admin.php';
require_once 'wp-functions/cleanup-public.php';
require_once 'wp-functions/cleanup-wpemoji.php';
require_once 'wp-functions/woo-fixes.php';
require_once 'wp-functions/woo-checkout-changes.php';

/**
 * Theme functions
 */
require_once 'wp-functions/ajax.php';
require_once 'wp-functions/admin-error.php';
require_once 'wp-functions/nav-menus.php';
require_once 'wp-functions/assets.php';
require_once 'wp-functions/image-sizes.php';
require_once 'wp-functions/custom-excerpt.php';
require_once 'wp-functions/menu-walkers.php';
require_once 'wp-functions/email-details.php';
require_once 'wp-functions/register-updates.php';
require_once 'wp-partials/post-teaser.php';
require_once 'wp-partials/post-teaser-small.php';
require_once 'wp-partials/product-teaser.php';
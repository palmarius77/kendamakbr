<footer id="footer">
  <div class="container">
    <div class="top-footer floating-objects centered-content">
      <p class="social-networks left">
        <span><?php _e( 'Follow us on', 'kendamakbr' ); ?>:</span>
				<?php $rep = get_field( 'socials', 'options' );
				foreach ( $rep as $it ) { ?>
          <a href="<?php echo $it['link']; ?>"><i class="fab <?php echo $it['class']; ?>"></i></a>
				<?php } ?>
      </p>

      <nav class="footer-menu">
				<?php
				wp_nav_menu( [
						'theme_location' => 'footer',
						'container'      => ''
				] );
				?>
      </nav>

      <p class="right"><?php _e( 'Contact', 'kendamakbr' ); ?>: <?php the_field( 'phone', 'options' ); ?> |
        <a href="mailto:<?php the_field( 'email', 'options' ); ?>"><?php the_field( 'email', 'options' ); ?></a>
      </p>
    </div>

    <div class="bottom-footer floating-objects">
			<?php $img = get_field( 'footer_payment_image', 'options' ); ?>
      <div class="cards left"><img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>"></div>
      <p class="right"><?php the_field( 'footer_copyright', 'options' ); ?></p>
    </div>
  </div>
</footer>
</div><!-- End #page -->
<?php wp_footer(); ?>
</body>
</html>
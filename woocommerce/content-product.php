<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<div class="slide-item">
  <div class="product-item-wrapper">
    <div <?php post_class(); ?>>
			<?php
			/**
			 * woocommerce_before_shop_loop_item hook.
			 *
			 * @hooked woocommerce_template_loop_product_link_open - 10 --> removed
			 */
			do_action( 'woocommerce_before_shop_loop_item' );

			/**
			 * woocommerce_before_shop_loop_item_title hook.
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10 -->removed
			 */
			do_action( 'woocommerce_before_shop_loop_item_title' );
			?>

      <div class="product-item">

        <div class="add-to-fav"><i class="far fa-heart add-to-wish-trigger"></i><?php echo do_shortcode( '[yith_wcwl_add_to_wishlist]' ); ?></div>

        <a href="<?php the_permalink(); ?>" class="image">
          <span class="f1_card">
            <span class="front face"><?php echo woocommerce_get_product_thumbnail( 'gallery_med' ); ?></span>
	          <?php
	          $attachment_ids = $product->get_gallery_image_ids();
	          if ( $attachment_ids && has_post_thumbnail() ) {
		          $attachment_id = $attachment_ids[0];
		          $thumbnail_src = wp_get_attachment_image_src( $attachment_id, 'gallery_med' ); ?>
              <span class="back face"><img src="<?php echo $thumbnail_src[0]; ?>" alt="gallery image"></span>
	          <?php } else { ?>
              <span class="back face"><?php echo woocommerce_get_product_thumbnail( 'gallery_med' ); ?></span>
	          <?php } ?>
          </span>
        </a>
				<?php
				/**
				 * woocommerce_shop_loop_item_title hook.
				 *
				 * @hooked woocommerce_template_loop_product_title - 10 --> removed
				 */
				do_action( 'woocommerce_shop_loop_item_title' );
				?>

        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

				<?php
				/**
				 * woocommerce_after_shop_loop_item_title hook.
				 *
				 * @hooked woocommerce_template_loop_rating - 5 --> removed
				 * @hooked woocommerce_template_loop_price - 10
				 */
				do_action( 'woocommerce_after_shop_loop_item_title' );

				/**
				 * woocommerce_after_shop_loop_item hook.
				 *
				 * @hooked woocommerce_template_loop_product_link_close - 5 --> removed
				 * @hooked woocommerce_template_loop_add_to_cart - 10
				 */
				do_action( 'woocommerce_after_shop_loop_item' );
				?>
      </div>

    </div>

  </div>
</div>

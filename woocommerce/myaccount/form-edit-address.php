<?php
/**
 * Edit address form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$page_title = ( 'billing' === $load_address ) ? __( 'Billing address', 'woocommerce' ) : __( 'Shipping address', 'woocommerce' );

do_action( 'woocommerce_before_edit_account_address_form' ); ?>

<?php if ( ! $load_address ) : ?>
	<?php wc_get_template( 'myaccount/my-address.php' ); ?>
<?php else : ?>

  <form method="post">

    <h3><?php echo apply_filters( 'woocommerce_my_account_edit_address_title', $page_title, $load_address ); ?></h3><?php // @codingStandardsIgnoreLine ?>

    <div class="woocommerce-address-fields">
			<?php do_action( "woocommerce_before_edit_address_form_{$load_address}" ); ?>

      <div class="woocommerce-address-fields__field-wrapper">
				<?php
				if ( $address['billing_first_name'] ) {
					$address['billing_first_name']['priority'] = 1;
					$address['billing_last_name']['priority']  = 2;
					$address['billing_email']['priority']      = 3;
					$address['billing_phone']['priority']      = 4;
					$address['billing_address_1']['priority']  = 5;
					$address['billing_street_nb']['priority']  = 6;
					$address['billing_address_2']['priority']  = 7;
					$address['billing_postcode']['priority']   = 8;
					$address['billing_country']['priority']    = 9;
					$address['billing_state']['priority']      = 10;
					$address['billing_city']['priority']       = 11;
					$address['billing_phone']['class']         = [ 'form-row-first' ];
					$address['billing_email']['class']         = [ 'form-row-last' ];
					$address['billing_address_1']['label']     = __( 'Address', 'kendamakbr' );
					$address['billing_address_1']['class']     = [ 'field-row-70' ];
					$address['billing_street_nb']['class']     = [ 'field-row-30' ];
				}
				if ( $address['shipping_company'] ) {
					$address['shipping_company']['priority']    = 1;
					$address['shipping_first_name']['priority'] = 2;
					$address['shipping_last_name']['priority']  = 3;
					$address['shipping_address_1']['priority']  = 5;
					$address['shipping_street_nb']['priority']  = 6;
					$address['shipping_address_2']['priority']  = 7;
					$address['shipping_postcode']['priority']   = 8;
					$address['shipping_country']['priority']    = 9;
					$address['shipping_state']['priority']      = 10;
					$address['shipping_city']['priority']       = 11;
					$address['shipping_address_1']['label']     = __( 'Address', 'kendamakbr' );
					$address['shipping_address_1']['class']     = [ 'field-row-70' ];
					$address['shipping_street_nb']['class']     = [ 'field-row-30' ];
				}

				$address = array_sort( $address, 'priority', SORT_ASC );
				foreach ( $address as $key => $field ) {
					if ( isset( $field['country_field'], $address[ $field['country_field'] ] ) ) {
						$field['country'] = wc_get_post_data_by_key( $field['country_field'], $address[ $field['country_field'] ]['value'] );
					}
					woocommerce_form_field( $key, $field, wc_get_post_data_by_key( $key, $field['value'] ) );
				}
				?>
      </div>

			<?php do_action( "woocommerce_after_edit_address_form_{$load_address}" ); ?>

      <p>
        <button type="submit" class="button" name="save_address"
                value="<?php esc_attr_e( 'Save address', 'woocommerce' ); ?>"><?php esc_html_e( 'Save address', 'woocommerce' ); ?></button>
				<?php wp_nonce_field( 'woocommerce-edit_address' ); ?>
        <input type="hidden" name="action" value="edit_address"/>
      </p>
    </div>

  </form>

<?php endif; ?>

<?php do_action( 'woocommerce_after_edit_account_address_form' ); ?>

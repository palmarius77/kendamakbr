<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see      https://docs.woocommerce.com/document/template-structure/
 * @author    WooThemes
 * @package  WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Hook Woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.

	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

  <div class="page-content floating-objects">

    <div class="product-gallery-wrapper">
			<?php
			/**
			 * Hook: woocommerce_before_single_product_summary.
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10 --> removed
			 * @hooked woocommerce_show_product_images - 20
			 */
			do_action( 'woocommerce_before_single_product_summary' );
			?>
    </div>

    <div class="product-text-details">
      <div class="floating-objects">
        <div class="product-details-left">
					<?php
					/**
					 * Hook: Woocommerce_single_product_summary.
					 *
					 * --> removed
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 * --> done
					 * @hooked WC_Structured_Data::generate_product_data() - 60
					 */
					woocommerce_template_single_title(); ?>

          <div class="floating-objects small-mb">
            <div class="left">
							<?php woocommerce_template_single_price(); ?>
            </div>
						<?php echo woocommerce_template_single_rating(); ?>
          </div>
        </div>
        <div class="product-details-right">
          <div class="grey-box">
            <div class="top-bar floating-objects">
							<?php woocommerce_template_single_sharing(); ?>
              <div class="add-to-fav right"><i class="far fa-heart"></i><?php echo do_shortcode( '[yith_wcwl_add_to_wishlist]' ); ?></div>
            </div>
            <div class="bottom-bar floating-objects">
							<?php woocommerce_template_single_add_to_cart(); ?>
            </div>
          </div>
        </div>
        <div class="clear"></div>
	      <?php if ( ! empty( $post->post_content ) ) { ?>
          <h4><?php _e( 'Description', 'kendamakbr' ); ?></h4>
		      <?php echo wpautop( $post->post_content ); ?>
	      <?php } ?>
      </div>

			<?php do_action( 'woocommerce_single_product_summary' ); ?>

    </div>

  </div><!-- End .page-content -->


  <div class="product-bottom">

		<?php
		comments_template();
		woocommerce_output_related_products();
		/**
		 * Hook: woocommerce_after_single_product_summary. --> all removed
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
		?>
  </div>

</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>

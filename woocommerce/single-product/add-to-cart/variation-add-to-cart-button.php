<?php
/**
 * Single variation cart button
 *
 * @see  https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>
<div class="woocommerce-variation-add-to-cart variations_button">
	<?php
	/**
	 * @since 3.0.0.
	 */
	do_action( 'woocommerce_before_add_to_cart_quantity' );

	$min = apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product );
	if ( $min == - 1 ) $min = 1;
	$max = apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product );
	if ( $max == - 1 ) $max = 3;

	echo '<div class="hidden_quantity">';
	woocommerce_quantity_input( array(
			'min_value'   => $min,
			'max_value'   => $max,
			'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : $product->get_min_purchase_quantity(),
	) );
	echo '</div>';

	/**
	 * @since 3.0.0.
	 */
	do_action( 'woocommerce_after_add_to_cart_quantity' );
	?>
  <div class="quantity">
    <h6><?php _e( 'Quantity', 'kendamakbr' ); ?></h6>
    <div class="select-block">
      <select name="quantity" class='custom-select quantity-select-trigger'>
				<?php for ( $i = $min; $i <= $max; $i ++ ) {
					echo '<option value="' . $i . '"';
					if ( isset( $_REQUEST[ 'quantity' ] ) && $_REQUEST[ 'quantity' ] == $i ) echo ' selected';
					echo '>' . $i . '</option>';
				} ?>
      </select>
    </div>
  </div>
  <button type="submit"
          class="single_add_to_cart_button button alt main-add ajax_add_to_cart add-to-cart"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
  <input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>"/>
  <input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>"/>
  <input type="hidden" name="variation_id" class="variation_id" value="0"/>
</div>

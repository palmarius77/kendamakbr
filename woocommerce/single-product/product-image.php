<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.2
 */

defined( 'ABSPATH' ) || exit;
// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}
global $product;
?>

<div class="product-slider-wrapper">

	<?php
	woocommerce_show_product_sale_flash();
	woocommerce_template_loop_no_stock();
	?>
  <!-- <i class="fas fa-search-plus"></i> -->

  <div class="product-slider">
		<?php
		$f_image          = wp_get_attachment_image_src( get_post_thumbnail_id( $product->ID ), 'gallery_med' );
		$f_image_full     = wp_get_attachment_image_src( get_post_thumbnail_id( $product->ID ), 'full_hd' );
		$f_image_full_img = wp_get_attachment_image_src( get_post_thumbnail_id( $product->ID ), 'full' );
		$f_image_small    = wp_get_attachment_image_src( get_post_thumbnail_id( $product->ID ), 'gallery_small' );

		// main image
		?>
    <div class="slide-item">
      <a href="<?php echo $f_image_full[0]; ?>"  data-fancybox="gallery"><img src="<?php echo $f_image[0]; ?>" alt="gallery image" class="zoom_image" data-zoom-image="<?php echo $f_image_full[0]; ?>"></a>
    </div>
		<?php
		// variation images
		if ( $product->is_type( 'variable' ) ) {
			$variations = $product->get_available_variations();
			foreach ( $variations as $variation ) {
				if ( $variation['image'] && $variation['attributes']['attribute_pa_color'] && $f_image_full_img[0] != $variation['image']['full_src'] ) {
					?>
          <div class="slide-item">
            <a href="<?php echo $variation['image']['src']; ?>"  data-fancybox="gallery"><img src="<?php echo $variation['image']['src']; ?>" alt="gallery image" class="zoom_image"
                 data-zoom-image="<?php echo $variation['image']['full_src']; ?>"></a>
          </div>
					<?php
				}
			}
		}
		$attachment_ids = $product->get_gallery_image_ids();
		if ( $attachment_ids && has_post_thumbnail() ) {
			foreach ( $attachment_ids as $attachment_id ) {
				$thumbnail_src = wp_get_attachment_image_src( $attachment_id, 'gallery_med' );
				$full_src      = wp_get_attachment_image_src( $attachment_id, 'full_hd' );
				?>
        <div class="slide-item">
          <a href="<?php echo $full_src[0]; ?>"  data-fancybox="gallery"><img src="<?php echo $thumbnail_src[0]; ?>" alt="gallery image" class="zoom_image" data-zoom-image="<?php echo $full_src[0]; ?>"></a>
        </div>
				<?php
			}
		}
		?>

  </div>

</div>

<div class="product-carousel">

  <div class="slide-item">
    <img src="<?php echo $f_image_small[0]; ?>" alt="gallery thumbnail">
  </div>

	<?php
	// variations images
	if ( $product->is_type( 'variable' ) ) {
		foreach ( $variations as $variation ) {
			if ( $variation['image'] && $variation['attributes']['attribute_pa_color'] && $f_image_full_img[0] != $variation['image']['full_src'] ) {
				?>
        <div class="slide-item color-var-<?php echo $variation['attributes']['attribute_pa_color']; ?>">
          <img src="<?php echo $variation['image']['gallery_thumbnail_src']; ?>" alt="gallery thumbnail">
        </div>
				<?php
			}
		}
	}
	// gallery images
	if ( $attachment_ids && has_post_thumbnail() ) {
		foreach ( $attachment_ids as $attachment_id ) {
			$small_src = wp_get_attachment_image_src( $attachment_id, 'gallery_small' );
			?>
      <div class="slide-item">
        <img src="<?php echo $small_src[0]; ?>" alt="gallery thumbnail">
      </div>
			<?php
		}
	}
	?>

</div>


<?php
// partial used as a function
function postTeaserSmall() { ?>
  <div class="slide-item">
    <article class="article-item">
      <a href="<?php the_permalink(); ?>" class="image" title="">
				<?php the_post_thumbnail( 'blog_thumb_2' ); ?>
        <span class="postdate"><i class="far fa-clock"></i> <?php echo get_the_date(); ?></span>
      </a>
      <div class="text-content">
        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
				<?php the_excerpt(); ?>
        <a href="<?php the_permalink(); ?>" class="button small"><?php _e( 'Read More', 'kendamakbr' ); ?></a>
      </div>
    </article>
  </div>
<?php } ?>
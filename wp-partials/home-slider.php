<div class="home-slider-wrapper">
  <div class="home-slider">

		<?php
		$rep = get_field( 'slider' );
		$i   = -1;
		foreach ( $rep as $it ):
			$i ++;
			$img_desk = $it['desktop_image']['sizes']['full_hd'];
			$img_mob  = $it['mobile_image']['sizes']['full_mob'];
			?>
      <div class="slide-item top-banner full-height"
           style="background-image: url(<?php echo $img_desk; ?>);"
           data-mobile="<?php if ( $img_mob ) echo $img_mob; else echo $img_desk; ?>">
				<?php if ( ! $img_desk ) {
					$video = $it['video'];
					$ext   = pathinfo( $video, PATHINFO_EXTENSION );
					?>
          <video autoplay muted loop id="theVideo_<?php echo $i; ?>">
            <source src="<?php echo $video; ?>" type="video/<?php echo $ext; ?>">
          </video>
				<?php } ?>
        <div class="container">
          <div class="inner-content">
            <div class="text-content left">
							<?php echo $it['text']; ?>
              <a href="<?php echo $it['button_link']; ?>" class="button"><?php echo $it['button_text']; ?></a>
            </div>
          </div>
        </div>
      </div>
		<?php endforeach; ?>

  </div>
</div>
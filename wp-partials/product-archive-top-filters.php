<div class="floating-objects small-mb">

	<?php $active_category = get_query_var( 'product_cat' ); ?>
  <ul class="products-categories-menu left">
    <li><a href="#" <?php if ( ! $active_category ) echo 'class="active"'; ?> data-slug="all"><?php _e( 'All', 'kendamakbr' ); ?></a></li>
		<?php $categories = get_terms( array(
				'taxonomy'   => 'product_cat',
				'hide_empty' => false,
				'parent'     => 15
		) );
		foreach ( $categories as $category ) { ?>
      <li><a href="#" <?php if ( $active_category == $category->slug ) echo 'class="active"'; ?>
             data-slug="<?php echo $category->slug; ?>"><?php echo $category->name; ?></a></li>
		<?php } ?>
  </ul>

	<?php woocommerce_result_count(); ?>

</div>

<div class="floating-objects options-bar">

  <div class="view-switch left">
    <div class="option">
      <span></span>
      <span></span>
      <span></span>
    </div>
    <div class="option">
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>

	<?php woocommerce_catalog_ordering(); ?>

</div>
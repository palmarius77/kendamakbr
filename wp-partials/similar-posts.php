<?php $categories = get_the_category();
if ( $categories[0] ): ?>

	<?php
	$args  = array(
			'post_type'      => 'post',
			'posts_per_page' => 6,
			'cat'            => $categories[0]->term_id,
			'orderby'        => 'rand',
			'post__not_in'   => [ get_the_ID() ]
	);
	$query = new WP_Query( $args );
	if ( $query->have_posts() ) { ?>

    <h4><?php _e( 'You May Also Like', 'kendamakbr' ); ?></h4>

    <div class="articles-listing small-mt">
      <div class="similar-articles-carousel">

				<?php while ( $query->have_posts() ) {
					$query->the_post();
					postTeaserSmall();
				} ?>

      </div>
    </div>

	<?php } ?>
<?php endif; ?>
<?php
global $td_uri;
if ( is_home() || is_tag() || is_category() ) {
	$id = get_option( 'page_for_posts' );
} else {
	$id = get_the_ID();
}
$title = get_the_title( $id );
if ( is_search() ) {
	$title = __( 'Search results for ', 'kendamakbr' ) . "'" . esc_html( wp_unslash( $_REQUEST['s'] ) ) . "'";
}
if ( is_404() ) {
	$title = __( '404', 'kendamakbr' );
}
if ( is_tag() ) {
	$title = __( 'Blog', 'kendamakbr' ) . ': ' . single_cat_title( "", false );
}
if ( is_category() ) {
	$title = __( 'Blog', 'kendamakbr' ) . ': ' . single_tag_title( "", false );
}
if ( is_post_type_archive( 'product' ) || is_shop() || is_product_category() || is_product_tag() ) {
	$title = __( 'Shop', 'kendamakbr' );
	$id    = get_field( 'shop_page', 'options' );
}
$size = get_field( 'top_size', $id );
if ( ! $size ) {
	$size = 'small';
}
$img     = get_field( 'top_image', $id );
$img_mob = get_field( 'top_image_mobile', $id );
if ( ! $img ) {
	$bg = $td_uri . '/images/general-banner.jpg';
} else {
	if ( $size == 'big' ) {
		$bg = $img['sizes']['full_hd'];
		if ( $img_mob ) {
			$bg_mob = $img_mob['sizes']['full_hd'];
		}
	} else {
		$bg = $img['sizes']['landscape'];
		if ( $img_mob ) {
			$bg_mob = $img_mob['sizes']['landscape'];
		}
	}
}
?>
<style type="text/css">
  .banner_bg {
    background-image: url(<?php echo $bg; ?>);
  }
</style>
<?php
if ( $img_mob ) { ?>
  <style type="text/css">
    @media (max-width: 767px) {
      .banner_bg {
        background-image: url(<?php echo $bg_mob; ?>);
      }
    }
  </style>
<?php } ?>
<div class="top-banner banner_bg <?php if ( $size == 'big' ) echo 'full-height'; ?>">
  <div class="container">
    <div class="inner-content">
      <div class="centered-content">
        <h1><?php echo $title; ?></h1>
      </div>
    </div>
  </div>
</div>
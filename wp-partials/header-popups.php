<div id="login-popup" style="display: none; max-width: 620px; width: 100%;">
  <div class="popup-content">
    <div class="centered-content"><h3><?php _e( 'My Account', 'kendamakbr' ); ?></h3></div>
    <div class="tabs-wrapper">
      <div class="navigator">
        <div class="tab-selector active"><?php _e( 'Login', 'kendamakbr' ); ?></div>
        <div class="tab-selector"><?php _e( 'Register', 'kendamakbr' ); ?></div>
      </div>
      <div class="tab-wrapper">
        <div class="tab active">

          <div class="lwa lwa-divs-only">
            <span class="lwa-status"></span>
            <form class="lwa-form" action="<?php echo esc_attr( LoginWithAjax::$url_login ); ?>" method="post">

              <div class="input-block lwa-username">
                <label><?php esc_html_e( 'Username/E-mail', 'login-with-ajax' ) ?></label>
                <input type="text" name="log" id="lwa_user_login" class="input" placeholder="e.g. john.smith@domain.com">
              </div>

              <div class="input-block lwa-password">
                <label><?php esc_html_e( 'Password', 'login-with-ajax' ) ?></label>
                <input type="password" name="pwd" id="lwa_user_pass" class="input" placeholder="">
              </div>

              <div class="lwa-login_form">
								<?php do_action( 'login_form' ); ?>
              </div>

              <div class="floating-objects">

                <div class="input-block left">
                  <input type="checkbox" id="remember_me" name="rememberme" class="styled-checkbox lwa-rememberme" value="forever">
                  <label for="remember_me"><?php esc_html_e( 'Remember me', 'login-with-ajax' ) ?></label>
                </div>

                <a class="lwa-links-remember right" href="<?php echo esc_attr( LoginWithAjax::$url_remember ); ?>"
                   title="<?php esc_attr_e( 'Forgot password?', 'login-with-ajax' ) ?>">
									<?php esc_attr_e( 'Forgot password?', 'login-with-ajax' ) ?>
                </a>

              </div>

              <div class="lwa-submit-button" style="float:none;">
                <input type="submit" class="button" name="wp-submit" id="lwa_wp-submit" value="<?php esc_attr_e( 'Login', 'login-with-ajax' ); ?>"
                       tabindex="100"/>
                <input type="hidden" name="login-with-ajax" value="login"/>
              </div>

            </form>

            <form class="lwa-remember" action="<?php echo esc_attr( LoginWithAjax::$url_remember ); ?>" method="post" style="display:none;">

              <p><strong><?php esc_html_e( "Forgot your password?", 'login-with-ajax' ); ?></strong></p>
              <p><?php esc_html_e( "Enter your emails bellow and a password reset will initiate.", 'login-with-ajax' ); ?></p>

              <div class="input-block lwa-remember-email">
                <input type="text" name="user_login" id="lwa_user_remember" value=""/>
								<?php do_action( 'lostpassword_form' ); ?>
              </div>

              <div class="lwa-submit-button">
                <input type="submit" value="<?php esc_attr_e( "Reset Password", 'login-with-ajax' ); ?>"/>
                <a href="#" class="lwa-links-remember-cancel" style="float: right; margin-top: 10px;"><?php esc_attr_e( "Cancel", 'login-with-ajax' ); ?></a>
                <input type="hidden" name="login-with-ajax" value="remember"/>
              </div>

            </form>

          </div>
        </div>

        <div class="tab">
          <div class="lwa lwa-divs-only">
            <span class="lwa-status"></span>
            <div class="lwa-register" style="display:block;">

              <form class="registerform" action="<?php echo esc_attr( LoginWithAjax::$url_register ); ?>" method="post">

                <div class="input-block lwa-username">
                  <label><?php _e( 'First Name', 'login-with-ajax' ); ?></label>
                  <input type="text" name="first_name" id="first_name" value="" placeholder="e.g. John"/>
                </div>

                <div class="input-block lwa-username">
                  <label><?php _e( 'Last Name', 'login-with-ajax' ); ?></label>
                  <input type="text" name="last_name" id="last_name" value="" placeholder="e.g. Smith"/>
                </div>

                <div class="input-block lwa-email">
                  <label><?php _e( 'E-mail (used as username)', 'login-with-ajax' ); ?></label>
                  <input type="text" name="user_email" id="user_email" value="" placeholder="e.g. john.smith@domain.com"/>
                  <input type="hidden" name="user_login" id="user_login" value=""/>
                  <script type="text/javascript">
                    jQuery(document).ready(function ($) {
                      $('.registerform #user_email').on('change', function () {
                        $('.registerform #user_login').val($(this).val());
                      });
                    });
                  </script>
                </div>

                <div class="input-block">
                  <label><?php _e( 'Password', 'login-with-ajax' ); ?></label>
                  <input type="password" name="password" value=""/>
                </div>
                <div class="input-block">
                  <label><?php _e( 'Confirm Password', 'login-with-ajax' ); ?></label>
                  <input type="password" name="password2" value="">
                </div>

								<?php do_action( 'register_form' ); ?>

                <p class="lwa-submit-button">
                  <input type="submit" name="wp-submit" id="wp-submit" class="button-primary" value="<?php esc_attr_e( 'Register', 'login-with-ajax' ); ?>"
                         tabindex="100"/>
                  <input type="hidden" name="login-with-ajax" value="register"/>
                </p>

              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
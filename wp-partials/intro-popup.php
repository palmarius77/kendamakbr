<div class="intro-popup inner-page">
	<?php
  global $td_uri;
	$img    = get_field( 'intro_screen_background', 'options' );
	?>
  <div class="top-banner full-height" style="background-image: url(<?php echo $img['sizes']['full_hd']; ?>);">
    <div class="container">
      <div class="inner-content">
        <a href="#" class="button white remove-intro">
          <img src="<?php echo $td_uri; ?>/images/english-flag.png" alt="EN">
					<?php _e( 'Go to english website', 'kendamakbr' ); ?>
        </a>
        <a href="<?php the_field('romanian_website', 'options'); ?>" class="button white remove-intro">
          <img src="<?php echo $td_uri; ?>/images/romanian-flag.png" alt="RO">
					<?php _e( 'Mergi la site-ul in romana', 'kendamakbr' ); ?>
        </a>
      </div>
    </div>
  </div>
</div>
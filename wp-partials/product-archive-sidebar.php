<a class="sidebar-toggle-bar" href="#">
  <span class="left"><?php _e( 'Show filters', 'kendamakbr' ); ?></span>
  <i class="fas fa-bars right"></i>
</a>

<div class="sidebar">
  <form method="GET" id="filter_form" action="<?php echo get_bloginfo( 'url' ); ?>/shop/">

		<?php
		$product_cat = get_query_var( 'product_cat' );
		$orderby     = $_GET['orderby'];
		$searched    = $_GET['s'];
		if ( isset( $_GET['price_range'] ) ) {
			$price_range = explode( ";", $_GET['price_range'] );
			$from        = $price_range[0];
			$to          = $price_range[1];
		}
		$active_brands = $_GET['brand'];
		$active_paints = $_GET['paint'];
		$active_woods  = $_GET['wood'];
		$active_colors = $_GET['color'];
		?>

    <input type="hidden" name="product_cat" id="product_cat_input" value="<?php echo $product_cat; ?>"
				<?php if ( ! isset( $product_cat ) || $product_cat == '' ) echo 'disabled' ?>>

		<?php if ( isset( $orderby ) && $orderby != '' ) { ?>
      <input type="hidden" name="orderby" id="orderby_input" value="<?php echo $orderby; ?>">
		<?php } ?>

		<?php if ( isset( $searched ) && $searched != '' ) { ?>
      <div class="widget">
        <h4 class="widgettitle"><span><?php _e( 'Filter by Search', 'kendamakbr' ); ?></span></h4>
        <div class="range-wrapper">
          <input type="text" name="s" id="search_input" value="<?php echo $searched; ?>">
          <p class="right"><a href="#" class="clear_search"><?php _e( 'clear search', 'kendamakbr' ); ?></a></p>
        </div>
      </div>
		<?php } ?>

    <div class="widget">
      <h4 class="widgettitle"><span><?php _e( 'Filter by Price', 'kendamakbr' ); ?></span></h4>
      <div class="range-wrapper">
        <input type="text" id="price_range" name="price_range" data-from="<?php echo $from; ?>" data-to="<?php echo $to; ?>">
      </div>
    </div>

    <div class="widget">
      <h4 class="widgettitle"><span><?php _e( 'Filter by Brand', 'kendamakbr' ); ?></span></h4>
      <ul class="filter-list">
				<?php $brands = get_terms( array(
						'taxonomy'   => 'brand',
						'hide_empty' => TRUE,
				) );
				foreach ( $brands as $brand ) { ?>
          <li>
            <div class="styled-checkbox-wrapper">
              <input type="checkbox" name="brand[]" value="<?php echo $brand->slug; ?>" class="styled-checkbox" id="brand_<?php echo $brand->slug; ?>"
									<?php if ( in_array( $brand->slug, $active_brands ) ) echo 'checked'; ?>>
              <label for="brand_<?php echo $brand->slug; ?>"><?php echo $brand->name; ?></label>
            </div>
            <div class="stock_no"><?php echo $brand->count; ?></div>
          </li>
				<?php } ?>
      </ul>
			<?php if ( count( $brands ) > 6 ) { ?>
        <a href="#" class="expand-list"><?php _e( 'Show More', 'kendamakbr' ); ?></a>
			<?php } ?>
    </div>

    <div class="widget">
      <h4 class="widgettitle"><span><?php _e( 'Filter by Color', 'kendamakbr' ); ?></span></h4>
      <ul class="filter-list">
				<?php $colors = get_terms( array(
						'taxonomy'   => 'pa_color',
						'hide_empty' => TRUE,
				) );
				foreach ( $colors as $color ) { ?>
          <li>
            <div class="styled-checkbox-wrapper">
              <input type="checkbox" name="color[]" value="<?php echo $color->slug; ?>" class="styled-checkbox" id="color_<?php echo $color->slug; ?>"
									<?php if ( in_array( $color->slug, $active_colors ) ) echo 'checked'; ?>>
              <label for="color_<?php echo $color->slug; ?>"><?php echo $color->name; ?></label>
            </div>
            <div class="stock_no"><?php echo $color->count; ?></div>
          </li>
				<?php } ?>
      </ul>
			<?php if ( count( $colors ) > 6 ) { ?>
        <a href="#" class="expand-list"><?php _e( 'Show More', 'kendamakbr' ); ?></a>
			<?php } ?>
    </div>

    <div class="widget">
      <h4 class="widgettitle"><span><?php _e( 'Filter by Paint', 'kendamakbr' ); ?></span></h4>
      <ul class="filter-list">
				<?php $paints = get_terms( array(
						'taxonomy'   => 'paint',
						'hide_empty' => TRUE,
				) );
				foreach ( $paints as $paint ) { ?>
          <li>
            <div class="styled-checkbox-wrapper">
              <input type="checkbox" name="paint[]" value="<?php echo $paint->slug; ?>" class="styled-checkbox" id="paint_<?php echo $paint->slug; ?>"
									<?php if ( in_array( $paint->slug, $active_paints ) ) echo 'checked'; ?>>
              <label for="paint_<?php echo $paint->slug; ?>"><?php echo $paint->name; ?></label>
            </div>
            <div class="stock_no"><?php echo $paint->count; ?></div>
          </li>
				<?php } ?>
      </ul>
			<?php if ( count( $paints ) > 6 ) { ?>
        <a href="#" class="expand-list"><?php _e( 'Show More', 'kendamakbr' ); ?></a>
			<?php } ?>
    </div>

    <div class="widget">
      <h4 class="widgettitle"><span><?php _e( 'Filter by Wood', 'kendamakbr' ); ?></span></h4>
      <ul class="filter-list">
				<?php $woods = get_terms( array(
						'taxonomy'   => 'wood',
						'hide_empty' => TRUE,
				) );
				foreach ( $woods as $wood ) { ?>
          <li>
            <div class="styled-checkbox-wrapper">
              <input type="checkbox" name="wood[]" value="<?php echo $wood->slug; ?>" class="styled-checkbox" id="wood_<?php echo $wood->slug; ?>"
									<?php if ( in_array( $wood->slug, $active_woods ) ) echo 'checked'; ?>>
              <label for="wood_<?php echo $wood->slug; ?>"><?php echo $wood->name; ?></label>
            </div>
            <div class="stock_no"><?php echo $wood->count; ?></div>
          </li>
				<?php } ?>
      </ul>
			<?php if ( count( $woods ) > 6 ) { ?>
        <a href="#" class="expand-list"><?php _e( 'Show More', 'kendamakbr' ); ?></a>
			<?php } ?>
    </div>

  </form>

</div>
<?php
global $cur, $cur_simb;
$rep      = get_field( 'products' );
$i        = - 1;
$home_url = get_home_url();
foreach ( $rep as $post ):
	setup_postdata( $post );
	$main_image_big   = get_field( 'main_image_big' );
	$main_image_wide  = get_field( 'main_image_wide' );
	$main_image_left  = get_field( 'main_image_left' );
	$main_image_right = get_field( 'main_image_right' );
	if ( ! $main_image_big || ! $main_image_wide || ! $main_image_left || ! $main_image_right ) {
		show_admin_error( 'missing_image' );
		continue;
	}
	$i ++;
	$product      = wc_get_product( $post->ID );
	$price_reg    = $product->get_regular_price();
	$price_sale   = $product->get_sale_price();
	$fancy_gal_id = 'gallery-' . $post->ID;
	?>
  <div class="split-section-wrapper" itemscope itemtype="http://schema.org/Product">
    <div class="main-product-part <?php if ( $i % 2 == 0 ) echo 'left'; else echo 'right'; ?>">
      <div class="inner-content">
        <a href="<?php echo $main_image_big['url']; ?>" class="image" data-fancybox="<?php echo $fancy_gal_id; ?>">
          <img src="<?php echo $main_image_big['sizes']['medium']; ?>" alt="<?php echo $main_image_big['alt']; ?>" itemprop="image">
        </a>
        <div class="text-content">
					<?php woocommerce_template_loop_price(); ?>
          <h3 itemprop="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
          <p itemprop="description"><?php echo $product->post->post_excerpt; ?></p>
          <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="button"><?php _e( 'View details', 'kendamakbr' ); ?></a>
          <?php /* ?>

					<?php if ( ! $product->has_child() ) { ?>
            <a href="<?php echo $home_url . '?add-to-cart=' . $post->ID; ?>" class="button add_to_cart_button home-add add-to-cart ajax_add_to_cart"
               data-quantity="1" data-product_id="<?php echo $post->ID; ?>" data-product_sku="" aria-label="Add “<?php the_title(); ?>” to your cart"
               rel="nofollow">
							<?php _e( 'Add to cart', 'kendamakbr' ); ?>
            </a>
					<?php } else { ?>
            <a href="<?php the_permalink(); ?>" class="button add_to_cart_button">
							<?php _e( 'Select options', 'kendamakbr' ); ?>
            </a>
					<?php } ?>
          
          */ ?>

        </div>
      </div>
    </div>
    <div class="images-gallery left">
      <div class="gallery-wrapper">
        <div class="gallery-item wide">
          <a href="<?php echo $main_image_wide['url']; ?>" class="image-wrapper" data-fancybox="<?php echo $fancy_gal_id; ?>"
             style="background-image: url(<?php echo $main_image_wide['sizes']['prod_thumb_wide']; ?>);"></a>
        </div>
        <div class="gallery-item">
          <a href="<?php echo $main_image_left['url']; ?>" title="" class="image-wrapper" data-fancybox="<?php echo $fancy_gal_id; ?>"
             style="background-image: url(<?php echo $main_image_left['sizes']['prod_thumb_med']; ?>);"></a>
        </div>
        <div class="gallery-item">
          <a href="<?php echo $main_image_right['url']; ?>" title="" class="image-wrapper" data-fancybox="<?php echo $fancy_gal_id; ?>"
             style="background-image: url(<?php echo $main_image_right['sizes']['prod_thumb_med']; ?>);"></a>
        </div>
      </div>
    </div>
  </div>
	<?php
endforeach;
wp_reset_postdata();
?>
<?php
// Basic page display
get_header();
wp_reset_query(); ?>

  <div class="about-page inner-page">

		<?php require_once 'wp-partials/top-banner.php'; ?>

    <div class="page-wrapper default-page">
      <div class="container">

				<?php require_once 'wp-partials/breadcrumb.php'; ?>

        <div class="page-content">
					<?php the_content(); ?>
        </div><!-- End .page-content -->


      </div>
    </div>

  </div>

<?php get_footer(); ?>
<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 */
get_header(); ?>

  <div class="about-page inner-page">

		<?php require_once 'wp-partials/top-banner.php'; ?>

    <div class="page-wrapper default-page">
      <div class="container">

				<?php require_once 'wp-partials/breadcrumb.php'; ?>

        <div class="page-content">
          <p><?php _e( 'Page not found', 'kendamakbr' ); ?></p>
        </div><!-- End .page-content -->


      </div>
    </div>

  </div>

<?php get_footer(); ?>
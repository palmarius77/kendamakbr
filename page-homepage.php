<?php
/*
Template Name: Homepage
*/
?>
<?php get_header(); ?>

  <div class="homepage">

		<?php require_once 'wp-partials/home-slider.php'; ?>

		<?php require_once 'wp-partials/home-products.php'; ?>

  </div>

<?php get_footer(); ?>
<?php
//Index file for displaying the blog
get_header();

$cat     = get_query_var( 'cat' );
$tagname = get_query_var( 'tag' );
$ppp   = 6;
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : '1';
?>

<div class="blog-page inner-page">

	<?php require_once 'wp-partials/top-banner.php'; ?>

  <div class="page-wrapper default-page">
    <div class="container">

			<?php require_once 'wp-partials/breadcrumb.php'; ?>

      <div class="page-content">
        <div class="row articles-listing ajax_posts">

					<?php
					$args  = array(
							'post_type'      => 'post',
							'posts_per_page' => $ppp,
							'cat'            => $cat,
							'tag'            => $tagname,
							'paged'          => $paged,
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) {
						while ( $query->have_posts() ) {
							$query->the_post();
							postTeaser();
						}
					} else {
						echo '<h2>' . __( 'No posts found', 'kendamakbr' ) . '</h2>';
					}
					?>

        </div>
				<?php
				if ( $cat ) {
					$category = get_category( $cat );
					$max      = $category->category_count;
				} elseif ( $tagname ) {
					$tag = get_term_by('name', $tagname, 'post_tag');
					$max      = $tag->count;
				} else {
					$count_posts = wp_count_posts( 'post' );
					$posts       = $count_posts->publish;
					$max         = $posts;
				}
				if ( $max > $ppp ):
					?>
          <div class="centered-content small-mt">
            <a href="#" title="" class="button load-more-btn" id="more_posts"
               data-category="<?php echo esc_attr( $cat ); ?>"
               data-tag="<?php echo esc_attr( $tagname ); ?>"
               data-ppp="<?php echo $ppp; ?>">
							<?php _e( 'Load More', 'kendamakbr' ); ?>
            </a>
          </div>
				<?php endif; ?>
      </div><!-- End .page-content -->

    </div>
  </div>

</div>

<?php get_footer(); ?>


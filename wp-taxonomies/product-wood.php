<?php
add_action( 'init', 'create_product_wood_taxonomy', 0 );

function create_product_wood_taxonomy() {
	$labels = array(
			'name'              => _x( 'Wood', 'taxonomy general name', 'kendamakbr' ),
			'singular_name'     => _x( 'Woods', 'taxonomy singular name', 'kendamakbr' ),
			'search_items'      => __( 'Search Woods', 'kendamakbr' ),
			'all_items'         => __( 'All Woods', 'kendamakbr' ),
			'parent_item'       => __( 'Parent Woods', 'kendamakbr' ),
			'parent_item_colon' => __( 'Parent Wood:', 'kendamakbr' ),
			'edit_item'         => __( 'Edit Wood', 'kendamakbr' ),
			'update_item'       => __( 'Update Wood', 'kendamakbr' ),
			'add_new_item'      => __( 'Add New Wood', 'kendamakbr' ),
			'new_item_name'     => __( 'New Wood Name', 'kendamakbr' ),
			'menu_name'         => __( 'Woods', 'kendamakbr' ),
	);

	$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array(
					'slug' => 'wood',
			),
	);

	register_taxonomy( 'wood', array( 'product' ), $args );
}


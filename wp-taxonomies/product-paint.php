<?php
add_action( 'init', 'create_product_paint_taxonomy', 0 );

function create_product_paint_taxonomy() {
	$labels = array(
			'name'              => _x( 'Paint', 'taxonomy general name', 'kendamakbr' ),
			'singular_name'     => _x( 'Paints', 'taxonomy singular name', 'kendamakbr' ),
			'search_items'      => __( 'Search Paints', 'kendamakbr' ),
			'all_items'         => __( 'All Paints', 'kendamakbr' ),
			'parent_item'       => __( 'Parent Paints', 'kendamakbr' ),
			'parent_item_colon' => __( 'Parent Paint:', 'kendamakbr' ),
			'edit_item'         => __( 'Edit Paint', 'kendamakbr' ),
			'update_item'       => __( 'Update Paint', 'kendamakbr' ),
			'add_new_item'      => __( 'Add New Paint', 'kendamakbr' ),
			'new_item_name'     => __( 'New Paint Name', 'kendamakbr' ),
			'menu_name'         => __( 'Paints', 'kendamakbr' ),
	);

	$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array(
					'slug' => 'paint',
			),
	);

	register_taxonomy( 'paint', array( 'product' ), $args );
}


<?php
add_action( 'init', 'create_product_brand_taxonomy', 0 );

function create_product_brand_taxonomy() {
	$labels = array(
			'name'              => _x( 'Brand', 'taxonomy general name', 'kendamakbr' ),
			'singular_name'     => _x( 'Brands', 'taxonomy singular name', 'kendamakbr' ),
			'search_items'      => __( 'Search Brands', 'kendamakbr' ),
			'all_items'         => __( 'All Brands', 'kendamakbr' ),
			'parent_item'       => __( 'Parent Brands', 'kendamakbr' ),
			'parent_item_colon' => __( 'Parent Brand:', 'kendamakbr' ),
			'edit_item'         => __( 'Edit Brand', 'kendamakbr' ),
			'update_item'       => __( 'Update Brand', 'kendamakbr' ),
			'add_new_item'      => __( 'Add New Brand', 'kendamakbr' ),
			'new_item_name'     => __( 'New Brand Name', 'kendamakbr' ),
			'menu_name'         => __( 'Brands', 'kendamakbr' ),
	);

	$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array(
					'slug' => 'brand',
			),
	);

	register_taxonomy( 'brand', array( 'product' ), $args );
}

